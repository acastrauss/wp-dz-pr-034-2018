﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WD_DZ_PR_034_2018.Models
{
    public class Kupovina
    {
        #region Constructors

        public Kupovina(Korisnik kupac, Dictionary<GramofonskaPloca, uint> izabranePloca, DateTime datumKupovine, uint ukupnoPlaceno)
        {
            Kupac = kupac;
            IzabranePloce = izabranePloca;
            DatumKupovine = datumKupovine;
            UkupnoPlaceno = ukupnoPlaceno;
        }

        public Kupovina()
        {
            Kupac = new Korisnik();
            IzabranePloce = new Dictionary<GramofonskaPloca, uint>();
            DatumKupovine = DateTime.Now;
            UkupnoPlaceno = 0;
        }

        public Kupovina(Kupovina refK) 
        {
            Kupac = refK.Kupac;
            IzabranePloce = refK.IzabranePloce;
            DatumKupovine = refK.DatumKupovine;
            UkupnoPlaceno = refK.UkupnoPlaceno;
        }

        #endregion

        #region Properties
        public Korisnik Kupac { get; set; }
        /// <summary>
        /// Ploce sa kolicinom
        /// </summary>
        public Dictionary<GramofonskaPloca, uint> IzabranePloce { get; set; }
        public DateTime DatumKupovine { get; set; }
        public uint UkupnoPlaceno { get; set; }

        #endregion

        #region AdditionalMethods

        public override string ToString()
        {
            String retVal = String.Format("{0}\n", Kupac.ToString());
            retVal += String.Format("{0}\n", DatumKupovine.ToString());

            foreach (var p in IzabranePloce)
            {
                retVal += "\n";
                retVal += String.Format("{0}\n", p);
            }

            retVal += String.Format("{0}\n", UkupnoPlaceno);

            return retVal;
        }

        public override bool Equals(object obj)
        {
            Kupovina kupovina = (Kupovina)obj;

            return
                this.Kupac.Equals(kupovina.Kupac) &&
                this.IzabranePloce.Equals(kupovina.IzabranePloce) &&
                this.DatumKupovine.Equals(kupovina.DatumKupovine) &&
                this.UkupnoPlaceno == kupovina.UkupnoPlaceno
                ;
        }

        #endregion
    }
}