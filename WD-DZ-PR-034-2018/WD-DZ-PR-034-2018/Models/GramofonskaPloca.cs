﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WD_DZ_PR_034_2018.Models
{
    public enum STANJE_PLOCE { POLOVNA, NOVA};

    public class GramofonskaPloca
    {
        #region Constructors
        public GramofonskaPloca(string naziv, string izvodjac, STANJE_PLOCE stanjePloce, string zanr, string opis, uint cena, uint brojKopija, string radnja)
        {
            Naziv = naziv;
            Izvodjac = izvodjac;
            StanjePloce = stanjePloce;
            Zanr = zanr;
            Opis = opis;
            Cena = cena;
            BrojKopija = brojKopija;
            Radnja = radnja;
        }

        public GramofonskaPloca() 
        {
            Naziv = String.Empty;
            Izvodjac = String.Empty;
            StanjePloce = STANJE_PLOCE.NOVA;
            Zanr = String.Empty;
            Opis = String.Empty;
            Cena = 0;
            BrojKopija = 0;
            Radnja = String.Empty;
        }

        public GramofonskaPloca(GramofonskaPloca refGP) 
        {
            this.Naziv = refGP.Naziv;
            this.Izvodjac = refGP.Izvodjac;
            this.StanjePloce = refGP.StanjePloce;
            this.Zanr = refGP.Zanr;
            this.Opis = refGP.Opis;
            this.Cena = refGP.Cena;
            this.BrojKopija = refGP.BrojKopija;
            Radnja = refGP.Radnja;
        }

        #endregion

        #region Properties

        public String Naziv { get; set; }
        public String Izvodjac { get; set; }
        public STANJE_PLOCE StanjePloce { get; set; }
        public String Zanr { get; set; }
        public String Opis { get; set; }
        public uint Cena { get; set; }
        public uint BrojKopija { get; set; }
        public String Radnja { get; set; }
        #endregion
        
        #region AdditionalMethods

        public override string ToString()
        {
            String retVal = String.Format("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}"
                , Naziv, Izvodjac, StanjePloce == STANJE_PLOCE.NOVA ? "Nova" : "Polovna", Zanr, Opis, Cena.ToString(), BrojKopija.ToString(), Radnja);

            return retVal;
        }

        public override bool Equals(object obj)
        {
            GramofonskaPloca gramofonskaPloca = (GramofonskaPloca)obj;

            return
                this.Naziv == gramofonskaPloca.Naziv &&
                this.Izvodjac == gramofonskaPloca.Izvodjac &&
                this.StanjePloce == gramofonskaPloca.StanjePloce &&
                this.Zanr == gramofonskaPloca.Zanr &&
                this.Cena == gramofonskaPloca.Cena &&
                this.Radnja == gramofonskaPloca.Radnja
                ;
        }

        public bool IsValid()
        {
            return
                this.Naziv.Length >= 1 &&
                this.Cena >= 0 && this.Cena <= 30000 &&
                this.BrojKopija >= 0 && this.BrojKopija <= 1000;
        }

        #endregion

    }
}