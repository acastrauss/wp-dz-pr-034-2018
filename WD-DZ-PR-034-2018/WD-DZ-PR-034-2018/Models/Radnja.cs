﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WD_DZ_PR_034_2018.Models
{
    public class Radnja
    {
        #region Constructors
        public Radnja(string naziv, string adresa, List<GramofonskaPloca> ploce)
        {
            Naziv = naziv;
            Adresa = adresa;
            Ploce = ploce;
        }

        public Radnja() 
        {
            Naziv = String.Empty;
            Adresa = String.Empty;
            Ploce = new List<GramofonskaPloca>();
        }

        public Radnja(Radnja refR) 
        {
            this.Naziv = refR.Naziv;
            this.Adresa = refR.Adresa;
            this.Ploce = refR.Ploce;
        }

        #endregion

        #region Properties

        public String Naziv { get; set; }
        public String Adresa { get; set; }
        public List<GramofonskaPloca> Ploce { get; set; }

        #endregion

        #region AdditionalMethods

        public override string ToString()
        {
            String retVal = String.Format("{0}\n{1}\n",
                Naziv, Adresa);

            foreach (var p in Ploce)
            {
                retVal += "\n";
                retVal += String.Format("{0}\n", p.ToString());
            }

            return retVal;

        }

        public bool IsValid()
        {
            return
                this.Naziv.Length >= 1;
        }

        #endregion
    }
}