﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using WD_DZ_PR_034_2018.Models;

namespace WD_DZ_PR_034_2018.Models
{
    public class FileAccess
    {
        public static List<Radnja> ReadShops()
        {
            List<Radnja> radnje = new List<Radnja>();

            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "radnje\\";
            
            var shopFiles = Directory.GetFiles(wd);

            foreach (var shopFile in shopFiles)
            {
                var path = Path.Combine(wd, String.Format("{0}", shopFile));
                FileStream stream = new FileStream(path, FileMode.Open);
                StreamReader sr = new StreamReader(stream);

                Radnja radnja = new Radnja();

                sr.ReadLine();
                sr.ReadLine();
                
                //var naziv = sr.ReadLine().Split(',');
                var naziv = sr.ReadLine().Split(',')[1];
                var adresa = sr.ReadLine().Split(',')[1];
                var obrisanaRadnja = sr.ReadLine().Split(',')[1].Equals("da") ? true : false;

                if(obrisanaRadnja) 
                {
                    sr.Close();
                    stream.Close();
                    continue;
                }

                radnja.Naziv = naziv;
                radnja.Adresa = adresa;

                // preskoci liniju
                sr.ReadLine();
                
                string line = "";

                while ((line = sr.ReadLine()) != null)
                {
                    line = sr.ReadLine();
                    if (line == null) break;
                    
                    var nazivPloce = sr.ReadLine().Split(',')[1];//sr.ReadLine().Split(',')[1];
                    var izvodjac = sr.ReadLine().Split(',')[1];
                    var stanje = sr.ReadLine().Split(',')[1];
                    var zanr = sr.ReadLine().Split(',')[1];
                    var opis = sr.ReadLine().Split(',')[1];
                    var cena = UInt32.Parse(sr.ReadLine().Split(',')[1]);
                    var obrisana = sr.ReadLine().Split(',')[1].Equals("ne") ? false : true;
                    var bk = UInt32.Parse(sr.ReadLine().Split(',')[1]);
                    var radnjaNaziv = sr.ReadLine().Split(',')[1];

                    if (obrisana) continue;

                    GramofonskaPloca ploca = new GramofonskaPloca(
                        nazivPloce, izvodjac, stanje.Equals("Nova") ? STANJE_PLOCE.NOVA : STANJE_PLOCE.POLOVNA,
                        zanr, opis, cena, bk, radnja.Naziv
                        );

                    radnja.Ploce.Add(ploca);
                }


                sr.Close();
                stream.Close();

                radnje.Add(radnja);
            }

            return radnje;
        }

        public static Dictionary<Korisnik, List<Kupovina>> ReadUsers()
        {
            var users = FileAccess.ReadUserType(ULOGA_KORISNIKA.ADMIN);

            FileAccess.ReadUserType(ULOGA_KORISNIKA.KUPAC).ToList().ForEach(x => users.Add(x.Key, x.Value));

            return users;
        }

        private static Dictionary<Korisnik, List<Kupovina>> ReadUserType(ULOGA_KORISNIKA uloga) 
        {
            Dictionary<Korisnik, List<Kupovina>> purchasesAll = new Dictionary<Korisnik, List<Kupovina>>();

            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += uloga == ULOGA_KORISNIKA.ADMIN ? "administratori\\" : "kupci\\";

            var adminFiles = Directory.GetFiles(wd);

            foreach (var adminFile in adminFiles)
            {
                var path = Path.Combine(wd, String.Format("{0}", adminFile));
                FileStream stream = new FileStream(path, FileMode.Open);
                StreamReader sr = new StreamReader(stream);

                sr.ReadLine();
                sr.ReadLine();

                //var naziv = sr.ReadLine().Split(',');
                var korisnickooIme = sr.ReadLine().Split(',')[1];
                var lozinka = sr.ReadLine().Split(',')[1];
                var ime = sr.ReadLine().Split(',')[1];
                var prezime = sr.ReadLine().Split(',')[1];
                var pol = sr.ReadLine().Split(',')[1];
                var email = sr.ReadLine().Split(',')[1];
                var datumRodjenja = DateTime.Parse(sr.ReadLine().Split(',')[1]);
                
                if(uloga == ULOGA_KORISNIKA.KUPAC) 
                {
                    var obirsan = sr.ReadLine().Split(',')[1].Contains("da") ? true : false;

                    if (obirsan)
                    {

                        sr.Close();
                        stream.Close();
                        continue;
                    }
                }

                Korisnik admin = new Korisnik(
                    korisnickooIme, lozinka, ime, prezime, pol.First(), email, datumRodjenja, uloga
                    );

                string line = "";

                // preskoci liniju Kupovine:
                line = sr.ReadLine();
                line = sr.ReadLine();

                List<Kupovina> kupovine = new List<Kupovina>();
                
                // preskoci praznu liniju
                sr.ReadLine();

                // citanje kupovina
                while ((line = sr.ReadLine()) != null)
                {
                    //line = sr.ReadLine();
                    if (line == null) break;

                    var str = sr.ReadLine();

                    var datumKupovine = DateTime.Parse(str.Split(',')[1]);
                    var ukupnoPlaceno = UInt32.Parse(sr.ReadLine().Split(',')[1]);

                    Dictionary<GramofonskaPloca, uint> ploce = new Dictionary<GramofonskaPloca, uint>();
                    // citanje ploca
                    sr.ReadLine(); // preskoci Ploce:
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains("Kupovina:") || String.IsNullOrEmpty(line)) break ;
                        // preskoci Ploca:
                        //line = sr.ReadLine();
                        //if (line == null) break;

                        var nazivPloce = sr.ReadLine().Split(',')[1];
                        var izvodjac = sr.ReadLine().Split(',')[1];
                        var stanje = sr.ReadLine().Split(',')[1].Equals("Nova") ? STANJE_PLOCE.NOVA : STANJE_PLOCE.POLOVNA;
                        var zanr = sr.ReadLine().Split(',')[1];
                        var opis = sr.ReadLine().Split(',')[1];
                        var cena = UInt32.Parse(sr.ReadLine().Split(',')[1]);
                        var bk = UInt32.Parse(sr.ReadLine().Split(',')[1]);
                        var obrisana = sr.ReadLine().Split(',')[1].Equals("ne") ? false : true;
                        var kol = UInt32.Parse(sr.ReadLine().Split(',')[1]);
                        var radnjaNaziv = sr.ReadLine().Split(',')[1];

                        if (obrisana) continue;

                        ploce.Add(
                            new GramofonskaPloca(nazivPloce, izvodjac, stanje, zanr, opis, cena, bk, radnjaNaziv), kol);

                    }

                    kupovine.Add(new Kupovina(
                        admin, ploce, datumKupovine, ukupnoPlaceno
                        ));
                }


                sr.Close();
                stream.Close();

                purchasesAll.Add(admin, kupovine);
            }

            return purchasesAll;

        }
    
        public static void RemoveUser(Korisnik korisnik) 
        {
            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "kupci\\";

            var userFiles = Directory.GetFiles(wd);

            foreach (var uf in userFiles)
            {
                if (!uf.Contains(korisnik.KorisnickoIme + ".csv")) continue;

                var path = Path.Combine(wd, String.Format("{0}", uf));
                FileStream stream = new FileStream(path, FileMode.Open);
                
                StreamReader sr = new StreamReader(stream);

                var lines = sr.ReadToEnd().Split('\n');
                sr.Close();
                stream.Close();
                // clear file
                File.WriteAllText(path, String.Empty);

                stream = new FileStream(path, FileMode.Open);

                StreamWriter sw = new StreamWriter(stream);

                for (int i = 0; i < lines.Length; i++)
                {
                    if(lines[i].Contains("Obrisan")) 
                    {
                        sw.Write(lines[i].Split(',')[0] + ", da\n");
                    }
                    else 
                    {
                        sw.Write(lines[i] + "\n");
                    }
                }

                sw.Close();
                stream.Close();

            }
        }

        public static void RemoveVinyl(GramofonskaPloca ploca, Radnja radnja) 
        {
            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "radnje\\";

            var userFiles = Directory.GetFiles(wd);

            foreach (var uf in userFiles)
            {
                var strs = uf.Split('\\')[7].Split('.')[0];

                if (!strs.Equals(radnja.Naziv))
                {
                    continue;
                }

                var path = Path.Combine(wd, String.Format("{0}", uf));
                FileStream stream = new FileStream(path, FileMode.Open);

                StreamReader sr = new StreamReader(stream);

                var lines = sr.ReadToEnd().Split('\n');
                sr.Close();
                stream.Close();
                // clear file
                File.WriteAllText(path, String.Empty);

                stream = new FileStream(path, FileMode.Open);

                StreamWriter sw = new StreamWriter(stream);

                string imePloce = "";

                for (int i = 0; i < lines.Length; i++)
                {
                    if(lines[i].Contains("Naziv")) 
                    {
                        imePloce = lines[i].Split(',')[1];
                        imePloce = imePloce.Substring(0, imePloce.Length - 1);
                        imePloce = imePloce != ploca.Naziv ? "" : imePloce;
                    }

                    bool promenjeno = false;

                    if(!String.IsNullOrEmpty(imePloce)) 
                    {
                        if (lines[i].Contains("Obrisana"))
                        {
                            sw.Write(lines[i].Split(',')[0] + ", da\n");
                            promenjeno = true;
                        }
                        
                    }
                    if (!promenjeno)
                    {
                        sw.Write(lines[i] + "\n");
                    }
                    
                }

                sw.Close();
                stream.Close();

                /*
                 # Radnja 

Naziv,Najbolje Ploce
Adresa,Nikole Pasica 10
Ploce:

Ploca:
Naziv,I was made for loving you
Izvodjac,Kiss
Stanje,Nova
Zanr,Rok
Opis,Fine pesme
Cena,3000
Broj Kopija,30
Obrisana,ne

Ploca:
Naziv,Layla and other love associate things
Izvodjac,Derek And Dominos
Stanje,Polovna
Zanr,Bluz Rok
Opis,Clapton is God
Cena,4500
Broj Kopija,40
Obrisana,ne
*/
            }
        }
    
        public static void RemoveShop(Radnja radnja)
        {

            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "radnje\\";

            var userFiles = Directory.GetFiles(wd);

            foreach (var uf in userFiles)
            {
                if (!uf.Contains(radnja.Naziv + ".csv")) continue;

                var path = Path.Combine(wd, String.Format("{0}", uf));
                FileStream stream = new FileStream(path, FileMode.Open);

                StreamReader sr = new StreamReader(stream);

                var lines = sr.ReadToEnd().Split('\n');
                if (lines.Length == 1)
                    lines = lines[0].Split('\r');

                sr.Close();
                stream.Close();
                // clear file
                File.WriteAllText(path, String.Empty);

                stream = new FileStream(path, FileMode.Open);

                StreamWriter sw = new StreamWriter(stream);

                int lineNum = 4;
                // 4 linije su pre obrisane
                for (int i = 0; i < lineNum; i++)
                {
                    sw.Write(lines[i] + '\n');
                }

                sw.Write("Obrisana,da\n");

                for (int i = lineNum + 1; i < lines.Length; i++)
                {
                    sw.Write(lines[i] + '\n');
                }

                sw.Close();
                stream.Close();

            }
        }
    
        public static void DecreaseVinylAmount(GramofonskaPloca ploca, Radnja radnja, uint kolicina) 
        {

            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "radnje\\";

            var userFiles = Directory.GetFiles(wd);

            foreach (var uf in userFiles)
            {
                if (!uf.Contains(radnja.Naziv + ".csv")) continue;

                var path = Path.Combine(wd, String.Format("{0}", uf));
                FileStream stream = new FileStream(path, FileMode.Open);

                StreamReader sr = new StreamReader(stream);

                var lines = sr.ReadToEnd().Split('\n');
                if (lines.Length == 1)
                    lines = lines[0].Split('\r');

                sr.Close();
                stream.Close();
                // clear file
                File.WriteAllText(path, String.Empty);

                stream = new FileStream(path, FileMode.Open);

                StreamWriter sw = new StreamWriter(stream);

                // linije pre ploca
                var linesBefore = 6;

                for (int i = 0; i < linesBefore; i++)
                {
                    sw.Write(lines[i] + '\n');
                }

                int j = linesBefore;

                while(j < lines.Length) 
                {
                    if(lines[j].Contains("Naziv"))
                    {
                        if(lines[j].Split(',')[1].Split('\r')[0] == ploca.Naziv) 
                        {
                            // 7 linija od naziva do broja kopija
                            for (int i = 0; i < 7; i++)
                            {
                                sw.Write(lines[j++] + '\n');
                            }
                            var brKopija = UInt32.Parse(lines[j].Split(',')[1]);
                            
                            if (brKopija < 0) brKopija = 0;

                            sw.Write("Broj Kopija," + (brKopija - kolicina).ToString() + '\n');
                        }
                        else
                        {
                            sw.Write(lines[j] + '\n');
                        }
                    }
                    else
                    {
                        sw.Write(lines[j] + '\n');
                    }

                    j++;
                }

                sw.Close();
                stream.Close();

            }
        }
    
        public static void AddRegisteredUser(Korisnik korisnik) 
        {
            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "kupci\\";

            var path = Path.Combine(wd, String.Format("{0}", korisnik.KorisnickoIme + ".csv"));
            FileStream stream = new FileStream(path, FileMode.CreateNew);

            StreamWriter sw = new StreamWriter(stream);

            sw.Write("Kupac:," + "\n");
            sw.Write("\n");
            sw.Write(String.Format("Korisnicko Ime,{0}\n", korisnik.KorisnickoIme));
            sw.Write(String.Format("Lozinka,{0}\n", korisnik.Sifra));
            sw.Write(String.Format("Ime,{0}\n", korisnik.Ime));
            sw.Write(String.Format("Prezime,{0}\n", korisnik.Prezime));
            sw.Write(String.Format("Pol,{0}\n", korisnik.Pol));
            sw.Write(String.Format("Email,{0}\n", korisnik.Email));
            sw.Write(String.Format("Datum Rodjenja,{0}\n", korisnik.DatumRodjenja.ToString()));
            sw.Write(String.Format("Obrisan,ne\n"));
            sw.Write("#Lista kupovina:\n");
            sw.Write("Kupovine:\n");

            sw.Close();
            stream.Close();
        }

        public static void AddShop(Radnja radnja)
        {
            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "radnje\\";

            var path = Path.Combine(wd, String.Format("{0}", radnja.Naziv + ".csv"));
            FileStream stream = new FileStream(path, FileMode.CreateNew);

            StreamWriter sw = new StreamWriter(stream);

            sw.Write("#Radnja\n");
            sw.Write("\n");
            sw.Write(String.Format("Naziv,{0}\n", radnja.Naziv));
            sw.Write(String.Format("Adresa,{0}\n", radnja.Adresa));
            sw.Write("Obrisana,ne\n");
            sw.Write("Ploce:\n");

            sw.Close();
            stream.Close();
        }
  
        public static void AddVinylToShop(GramofonskaPloca ploca, Radnja radnja) 
        {
            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "radnje\\";
            
            var shopFiles = Directory.GetFiles(wd);

            foreach (var sf in shopFiles)
            {
                if (!sf.Contains(radnja.Naziv + ".csv")) continue;

                var path = Path.Combine(wd, String.Format("{0}", radnja.Naziv + ".csv"));


                FileStream stream = new FileStream(path, FileMode.Open);

                StreamReader sr = new StreamReader(stream);

                var lines = sr.ReadToEnd().Split('\n');
                if (lines.Length == 1)
                    lines = lines[0].Split('\r');

                sr.Close();
                stream.Close();

                File.WriteAllText(path, String.Empty);

                stream = new FileStream(path, FileMode.Open);

                StreamWriter sw = new StreamWriter(stream);

                for (int i = 0; i < lines.Length - 1; i++)
                {
                    sw.Write(lines[i] + '\n');
                }
                sw.Write(lines[lines.Length - 1]);

                sw.Write('\n');
                sw.Write("Ploca:\n");
                sw.Write(String.Format("Naziv,{0}\n", ploca.Naziv));
                sw.Write(String.Format("Izvodjac,{0}\n", ploca.Izvodjac));
                sw.Write(String.Format("Stanje,{0}\n", ploca.StanjePloce == STANJE_PLOCE.NOVA ? "Nova" : "Polovna"));
                sw.Write(String.Format("Zanr,{0}\n", ploca.Zanr));
                sw.Write(String.Format("Opis,{0}\n", ploca.Opis));
                sw.Write(String.Format("Cena,{0}\n", ploca.Cena.ToString()));
                sw.Write(String.Format("Obirana,ne\n"));
                sw.Write(String.Format("Broj Kopija,{0}\n", ploca.BrojKopija));
                sw.Write(String.Format("Radnja,{0}\n", ploca.Radnja));

                sw.Close();
                stream.Close();

            }
        }

        public static void AddPurchase(Kupovina kupovina) 
        {
            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += kupovina.Kupac.Uloga == ULOGA_KORISNIKA.ADMIN ? "administratori\\" : "kupci\\";

            var userFiles = Directory.GetFiles(wd);

            foreach (var sf in userFiles)
            {
                if (!sf.Contains(kupovina.Kupac.KorisnickoIme + ".csv")) continue;

                var path = Path.Combine(wd, String.Format("{0}", kupovina.Kupac.KorisnickoIme + ".csv"));

                FileStream stream = new FileStream(path, FileMode.Open);

                StreamReader sr = new StreamReader(stream);

                var lines = sr.ReadToEnd().Split('\n');
                if (lines.Length == 1)
                    lines = lines[0].Split('\r');

                sr.Close();
                stream.Close();

                File.WriteAllText(path, String.Empty);

                stream = new FileStream(path, FileMode.Open);

                StreamWriter sw = new StreamWriter(stream);

                for (int i = 0; i < lines.Length; i++)
                {
                    sw.Write(lines[i] + '\n');
                }

                sw.Write("Kupovina:\n");
                sw.Write(String.Format("Dattum,{0}\n", kupovina.DatumKupovine.ToString()));
                sw.Write(String.Format("Ukupno Placeno,{0}\n", kupovina.UkupnoPlaceno.ToString()));
                sw.Write("Ploce:\n");

                var ploce = kupovina.IzabranePloce.Keys.ToList();

                for (int i = 0; i < ploce.Count; i++)
                {
                    var p = ploce[i];

                    sw.Write("Ploca:\n");
                    sw.Write(String.Format("Naziv,{0}\n", p.Naziv));
                    sw.Write(String.Format("Izvodjac,{0}\n", p.Izvodjac));
                    sw.Write(String.Format("Stanje,{0}\n", p.StanjePloce == STANJE_PLOCE.NOVA ? "Nova" : "Polovna"));
                    sw.Write(String.Format("Zanr,{0}\n", p.Zanr));
                    sw.Write(String.Format("Opis,{0}\n", p.Opis));
                    sw.Write(String.Format("Cena,{0}\n", p.Cena.ToString()));
                    sw.Write(String.Format("Broj Kopija,{0}\n", p.BrojKopija.ToString()));
                    sw.Write(String.Format("Obrisana,ne\n"));
                    sw.Write(String.Format("Kolicina,{0}\n", kupovina.IzabranePloce[p]));
                    sw.Write(String.Format("Radnja,{0}\n", p.Radnja));
                }

                sw.Close();
                stream.Close();
            }
        }

        public static void ChangeVinyl(GramofonskaPloca novaPloca, GramofonskaPloca staraPloca)
        {
            String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");//Directory.GetCurrentDirectory();
            wd += "radnje\\";

            var userFiles = Directory.GetFiles(wd);

            foreach (var sf in userFiles)
            {
                if (!sf.Contains(novaPloca.Radnja + ".csv")) continue;

                var path = Path.Combine(wd, String.Format("{0}", novaPloca.Radnja + ".csv"));

                FileStream stream = new FileStream(path, FileMode.Open);

                StreamReader sr = new StreamReader(stream);

                var lines = sr.ReadToEnd().Split('\n');
                if (lines.Length == 1)
                    lines = lines[0].Split('\r');

                sr.Close();
                stream.Close();

                File.WriteAllText(path, String.Empty);

                stream = new FileStream(path, FileMode.Open);

                StreamWriter sw = new StreamWriter(stream);

                uint linesBefore = 6;

                for (int i = 0; i < linesBefore; i++)
                {
                    sw.Write(lines[i] + '\n');
                }

                int j = (int)linesBefore;

                while (j < lines.Length)
                {
                    sw.Write(lines[j++] + '\n');   // 

                    if (!(j < lines.Length)) break;

                    sw.Write(lines[j++] + '\n');   // Ploca:

                    if (!(j < lines.Length)) break;

                    if (lines[j].Split(',')[1].Split('\r')[0] == staraPloca.Naziv) 
                    {
                        sw.Write(String.Format("Naziv,{0}\n", novaPloca.Naziv));
                        sw.Write(String.Format("Izvodjac,{0}\n", novaPloca.Izvodjac));
                        sw.Write(String.Format("Stanje,{0}\n", novaPloca.StanjePloce == STANJE_PLOCE.NOVA ? "Nova" : "Polovna"));
                        sw.Write(String.Format("Zanr,{0}\n", novaPloca.Zanr));
                        sw.Write(String.Format("Opis,{0}\n", novaPloca.Opis));
                        sw.Write(String.Format("Cena,{0}\n", novaPloca.Cena.ToString()));
                        sw.Write(String.Format("Obrisana,ne\n"));
                        sw.Write(String.Format("Broj Kopija,{0}\n", novaPloca.BrojKopija.ToString()));
                        sw.Write(String.Format("Radnja,{0}\n", novaPloca.Radnja));

                        j += 9;
                    }
                    else
                    {
                        // kopiranje ostalih ploca
                        int oneVinylLen = 9;

                        for (int i = 0; i < oneVinylLen; i++)
                        {
                            sw.Write(lines[j++] + '\n');
                        }
                    }
                }

                sw.Close();
                stream.Close();
            }
        }
    }
}