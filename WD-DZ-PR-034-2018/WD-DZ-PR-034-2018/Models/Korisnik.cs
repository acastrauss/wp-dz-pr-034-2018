﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WD_DZ_PR_034_2018.Models
{
    public enum ULOGA_KORISNIKA { ADMIN, KUPAC };

    public class Korisnik
    {
        
        #region Constructors

        public Korisnik(string korisnickoIme, string sifra, string ime, string prezime, char pol, string email, DateTime datumRodjenja, ULOGA_KORISNIKA uloga)
        {
            KorisnickoIme = korisnickoIme;
            Sifra = sifra;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Email = email;
            DatumRodjenja = datumRodjenja;
            Uloga = uloga;
        }

        public Korisnik()
        {
            KorisnickoIme = String.Empty;
            Sifra = String.Empty;
            Ime = String.Empty;
            Prezime = String.Empty;
            Pol = ' ';
            Email = String.Empty;
            DatumRodjenja = DateTime.Now;
            Uloga = ULOGA_KORISNIKA.KUPAC;
        }

        public Korisnik(Korisnik refK)
        {
            this.KorisnickoIme = refK.KorisnickoIme;
            this.Sifra = refK.Sifra;
            this.Ime = refK.Ime;
            this.Prezime = refK.Prezime;
            this.Pol = refK.Pol;
            this.Email = refK.Email;
            this.DatumRodjenja = refK.DatumRodjenja;
            this.Uloga = refK.Uloga;
        }

        #endregion

        #region Properties
        public String KorisnickoIme { get; set; }
        public String Sifra { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public Char Pol { get; set; }
        public String Email { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public ULOGA_KORISNIKA Uloga { get; set; }

        #endregion

        #region AdditionalMetohds

        public override string ToString()
        {
            String retVal = String.Empty;

            retVal += String.Format("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n"
                , KorisnickoIme, Sifra, Ime, Prezime, Pol, Email, DatumRodjenja, Uloga == ULOGA_KORISNIKA.ADMIN ? "Admin" : "Kupac");

            return retVal;
        }

        public override bool Equals(object obj)
        {
            Korisnik korisnik = (Korisnik)obj;

            return this.KorisnickoIme == korisnik.KorisnickoIme
                ;
        }

        public bool IsValid()
        {
            return this.KorisnickoIme.Length >= 3 &&
                this.Sifra.Length >= 8;
        }
        #endregion

    }
}