﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WD_DZ_PR_034_2018.Models;

namespace WD_DZ_PR_034_2018.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            var korpa = (Dictionary<GramofonskaPloca, uint>)HttpContext.Session["korpa"];
            ViewBag.ukupnaCena = korpa.Keys.Sum(x => x.Cena * korpa[x]);

            return View();
        }

        [HttpPost]
        public ActionResult AddToCart(GramofonskaPloca ploca)
        {
            var korpa = (Dictionary<GramofonskaPloca, uint>)HttpContext.Session["korpa"];

            if (korpa != null)
            {
                var foundKey = korpa.Keys.ToList().Find(x => x.Naziv == ploca.Naziv && x.StanjePloce == ploca.StanjePloce);
                if ( foundKey != null)
                {
                    korpa[foundKey]++;
                }
                else
                {
                    korpa.Add(ploca, 1);
                }
            }

            HttpContext.Session["korpa"] = (object)korpa;
            ViewBag.ukupnaCena = korpa.Keys.Sum(x => x.Cena * korpa[x]);

            return RedirectToAction("Index", "Shop");
        }

        public ActionResult CheckOut(uint ukupnaCena) 
        {
            var korisnik = ((Korisnik)HttpContext.Session["korisnik"]);

            var kupljenePloce = ((Dictionary<GramofonskaPloca, uint>)HttpContext.Session["korpa"]);

            var radnje = ((List<Radnja>)HttpContext.Application["radnje"]);

            foreach (var r in radnje)
            {
                foreach (var kp in kupljenePloce)
                {
                    if(r.Ploce.Contains(kp.Key) && r.Naziv == kp.Key.Radnja) 
                    {
                        r.Ploce.Remove(kp.Key);
                        kp.Key.BrojKopija -= kupljenePloce[kp.Key];
                        if (kp.Key.BrojKopija < 0) kp.Key.BrojKopija = 0;
                        r.Ploce.Add(new GramofonskaPloca(kp.Key));
                    }
                }
            }

            var ploce = ((Dictionary<GramofonskaPloca, uint>)HttpContext.Session["korpa"]);

            var datum = DateTime.Now;

            Kupovina kupovina = new Kupovina(korisnik, ploce, datum, ukupnaCena);

            ((List<Kupovina>)HttpContext.Session["kupovine"]).Add(kupovina);

            HttpContext.Session["korpa"] = (object)new Dictionary<GramofonskaPloca, uint>();


            foreach (var ploca in kupljenePloce)
            {
                FileAccess.DecreaseVinylAmount(ploca.Key, ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == ploca.Key.Radnja), ploca.Value);
            }

            FileAccess.AddPurchase(kupovina);

            return RedirectToAction("Index", "Home");
        }
    }
}