﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WD_DZ_PR_034_2018.Models;

namespace WD_DZ_PR_034_2018.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            bool appInit = HttpContext.Application["init"] == null ? false : true;
            
            if(!appInit)
                this.InitApplication();

            return View();
        }

        private void InitApplication() 
        {
            HttpContext.Application["init"] = true;
            
            HttpContext.Application["radnje"] = (object)FileAccess.ReadShops();

            var korisniciKupovine = FileAccess.ReadUsers();

            HttpContext.Application["korisnici"] = (object)korisniciKupovine.Keys.ToList();
            HttpContext.Application["kupovineSvih"] = (object)korisniciKupovine;
            HttpContext.Session["korisnik"] = null;
            HttpContext.Session["korpa"] = null;
            HttpContext.Session["kupovine"] = null;
        }

        public ActionResult Register() 
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Models.Korisnik korisnik)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];

            if (korisnici.Contains(korisnik))
            {
                ViewBag.msg = String.Format("Korisnik {0} vec postoji.", korisnik.KorisnickoIme);
                return View();
            }
            else if(!korisnik.IsValid())
            {
                ViewBag.msg = "Nevalidne vrednosti za korisnika.";
                return View();
            }
            else 
            {
                ((List<Korisnik>)HttpContext.Application["korisnici"]).Add(korisnik);
                HttpContext.Session["korisnik"] = korisnik;

                if(((Dictionary<Korisnik, List<Kupovina>>)HttpContext.Application["kupovineSvih"]).ContainsKey(korisnik)) 
                {
                    HttpContext.Session["kupovine"] = ((Dictionary<Korisnik, List<Kupovina>>)HttpContext.Application["kupovineSvih"]);
                }
                else
                {
                    HttpContext.Session["kupovine"] = new Dictionary<Korisnik, List<Kupovina>>();
                }

                // Ploce sa kolicinom
                HttpContext.Session["korpa"] = new Dictionary<GramofonskaPloca, uint>();

                // Dodaj u podatke
                FileAccess.AddRegisteredUser(korisnik);

                return View("Index");
            }
        }

        public ActionResult LogIn() 
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string korisnickoIme, string sifra)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];

            var korisnik = korisnici.Find(x => (x.KorisnickoIme == korisnickoIme && x.Sifra == sifra));

            if (korisnik != null)
            {   // LogIn succesfull

                // Ploce sa kolicinom
                HttpContext.Session["korpa"] = new Dictionary<GramofonskaPloca, uint>();

                HttpContext.Session["korisnik"] = korisnik;
                HttpContext.Session["kupovine"] = ((Dictionary<Korisnik, List<Kupovina>>)HttpContext.Application["kupovineSvih"])[korisnik];

                return View("Index");
            }
            else
            {
                ViewBag.msg = String.Format("Neuspesno prijavljivanje.");
                return View();
            }
        }

        public ActionResult LogOut() 
        {
            HttpContext.Session["korisnik"] = null;
            return View("Index");
        }

        [HttpPost]
        public ActionResult DeleteUser(string userName)
        {
            var korisnik = ((List<Korisnik>)HttpContext.Application["korisnici"]).Find(x => x.KorisnickoIme == userName);
            ((List<Korisnik>)HttpContext.Application["korisnici"]).Remove(korisnik);

            FileAccess.RemoveUser(korisnik);

            return View("Index");
        }

        public ActionResult AddShop() 
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddShop(Radnja radnja) 
        {
            if (((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == radnja.Naziv && x.Adresa == radnja.Adresa) != null) 
            {
                ViewBag.msg = "Vec postoji takva radnja";
                return View();
            }
            else if(!radnja.IsValid())
            {
                ViewBag.msg = "Nevalidne vrednosti za radnju";
                return View();
            }
            else 
            {
                ((List<Radnja>)HttpContext.Application["radnje"]).Add(radnja);

                // dodaj u fajl
                FileAccess.AddShop(radnja);

                return View("Index");
            }
        }

    }
}