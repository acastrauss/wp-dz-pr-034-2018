﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WD_DZ_PR_034_2018.Models;

namespace WD_DZ_PR_034_2018.Controllers
{
    public class ShopController : Controller
    {

        public ActionResult Index()
        {
            string shopName = (string)HttpContext.Cache["imeProdavnice"];

            List<Radnja> radnje = (List<Radnja>)HttpContext.Application["radnje"];

            if (radnje != null)
            {
                Radnja radnja = radnje.Find(x => x.Naziv == shopName);
                ViewBag.radnja = radnja;
                ViewBag.msg = shopName;
                ViewBag.ploceRadnje = radnja.Ploce;
            }
            else
            {
                ViewBag.msg = "Nema te radnje!";
            }

            return View();
        }
        
        [HttpPost]
        public ActionResult Index(string shopName)
        {
            List<Radnja> radnje = (List<Radnja>)HttpContext.Application["radnje"];

            if(radnje != null) 
            {
                Radnja radnja = radnje.Find(x => x.Naziv == shopName);
                ViewBag.ploceRadnje = radnja.Ploce;
                ViewBag.radnja = radnja;
                ViewBag.msg = shopName;
                
            }
            else
            {
                ViewBag.msg = "Nema te radnje!";
            }

            // da moze da se vrati na prodavnicu
            HttpContext.Cache["imeProdavnice"] = (object)shopName;

            return View();
        }

        [HttpPost]
        public ActionResult DeleteShop(string shopName) 
        {
            var shop = ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName);
            ((List<Radnja>)HttpContext.Application["radnje"]).Remove(shop);
            // izbrisi logicki u fajlu
            FileAccess.RemoveShop(shop);

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult DeletePloca(string gpName)
        {
            string shopName = (string)HttpContext.Cache["imeProdavnice"];

            var ploca = ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName).Ploce.Find(x => x.Naziv == gpName);


            ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName).Ploce.Remove(ploca);

            FileAccess.RemoveVinyl(ploca, ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName));
            
            return RedirectToAction("Index", "Shop");
        }

        [HttpPost]
        public ActionResult SortData(string tipSortiranja)
        {
            string shopName = (string)HttpContext.Cache["imeProdavnice"];

            Radnja radnja = ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName);

            ViewBag.radnja = radnja;
            ViewBag.msg = shopName;
            
            switch (tipSortiranja)
            {
                case "rastucePoNazivu":
                    ViewBag.ploceRadnje = radnja.Ploce.OrderBy(x => x.Naziv).ToList();
                    break;
                
                case "opadajucePoNazivu":
                    ViewBag.ploceRadnje = radnja.Ploce.OrderByDescending(x => x.Naziv).ToList();
                    break;
                
                case "rastucePoIzvodjacu":
                    ViewBag.ploceRadnje = radnja.Ploce.OrderBy(x => x.Izvodjac).ToList();
                    break;

                case "opadajucePoIzvodjacu":
                    ViewBag.ploceRadnje = radnja.Ploce.OrderByDescending(x => x.Izvodjac).ToList();
                    break;

                case "rastucePoCeni":
                    ViewBag.ploceRadnje = radnja.Ploce.OrderBy(x => x.Cena).ToList();
                    break;
                
                case "opadajucePoCeni":
                    ViewBag.ploceRadnje = radnja.Ploce.OrderByDescending(x => x.Cena).ToList();
                    break;

                default:
                    break;
            }

            return View("Index");
        }
    
        public ActionResult FilterData(string tipFiltriranja, string param = "", int pocetnaCena = -1, int krajnjaCena = Int32.MaxValue) 
        {
            string shopName = (string)HttpContext.Cache["imeProdavnice"];

            Radnja radnja = ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName);

            ViewBag.radnja = radnja;
            ViewBag.msg = shopName;

            switch (tipFiltriranja)
            {
                case "naziv":
                    ViewBag.ploceRadnje = radnja.Ploce.FindAll(x => x.Naziv.Contains(param));
                    break;

                case "izvodjac":
                    ViewBag.ploceRadnje = radnja.Ploce.FindAll(x => x.Izvodjac.Contains(param));
                    break;

                case "cena":
                    ViewBag.ploceRadnje = radnja.Ploce.FindAll(x => x.Cena >= pocetnaCena && x.Cena <= krajnjaCena);
                    break;
                
                default:
                    break;
            }

            return View("Index");
        }
        
        public ActionResult AddPloca()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddPloca(GramofonskaPloca ploca) 
        {
            string shopName = (string)HttpContext.Cache["imeProdavnice"];
            ploca.Radnja = shopName;
            var radnje = ((List<Radnja>)HttpContext.Application["radnje"]);

            Radnja radnja = ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName);

            HttpContext.Cache["imeProdavnice"] = shopName;

            if (radnja.Ploce.Find(x=> x.Equals(ploca))!= null) 
            {
                ViewBag.msg = "Vec postoji takva ploca!";
                return View();
            }
            else if (!ploca.IsValid())
            {
                ViewBag.msg = "Nevalidna ploca.";
                return View();
            }
            else
            {
                radnje.Remove(radnja);
                radnja.Ploce.Add(ploca);
                radnje.Add(radnja);

                HttpContext.Application["radnje"] = (object)radnje;

                FileAccess.AddVinylToShop(ploca, radnja);

                return RedirectToAction("Index", "Shop");
            }
        }

        public ActionResult ChangePloca(string gpName) 
        {
            string shopName = (string)HttpContext.Cache["imeProdavnice"];

            // zapamti koju plocu da promeni
            var plocaZaPromenu = ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName).Ploce.Find(x => x.Naziv == gpName);
            
            HttpContext.Cache["plocaZaPromenu"] = plocaZaPromenu;
            ViewBag.plocaZaPromenu = plocaZaPromenu;

            return View();
        }

        [HttpPost]
        public ActionResult ChangePloca(GramofonskaPloca ploca) 
        {
            if(!ploca.IsValid())
            {
                ViewBag.msg = "Nevalidna ploca.";
                return View();
            }
            else
            {
                string shopName = (string)HttpContext.Cache["imeProdavnice"];
                ploca.Radnja = shopName;
                ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName).Ploce.Remove((GramofonskaPloca)HttpContext.Cache["plocaZaPromenu"]);
                ((List<Radnja>)HttpContext.Application["radnje"]).Find(x => x.Naziv == shopName).Ploce.Add(ploca);

                FileAccess.ChangeVinyl(ploca, (GramofonskaPloca)HttpContext.Cache["plocaZaPromenu"]);

                return RedirectToAction("Index", "Shop");
            }
        }
    }
}